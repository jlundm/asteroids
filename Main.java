

import controller.GameController;
import model.api.Api;
import view.GameView;
import view.Window;

public class Main {
	
	public static void main(String[] args) {
		Window window = new Window();
		Api api = new Api();
		GameView gameView = new GameView();
		GameController controller = new GameController(api, window, gameView);
		controller.initializeGame();
	}

}
