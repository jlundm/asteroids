package controller;

import java.awt.Window;

import model.api.Api;
import model.api.GameStateChangeObserver;
import view.GameView;

public class GameController implements GameStateChangeObserver {
	private GameView gameView;
	private Window window;
	private Api api;
	private KeyboardHandler keyboardHandler;
	
	public GameController(Api api, Window window, GameView gameView) {
		this.api = api;
		this.window = window;
		this.gameView = gameView;
		api.addGameStateChangeObserver(this);
	}
	
	public void initializeGame() {
		window.add(gameView);
		window.pack();
		window.repaint();
		keyboardHandler = new KeyboardHandler(api);
		window.addKeyListener(keyboardHandler);
		gameView.drawLevelLoadedScreen(api.getLevelNumber());
	}

	@Override
	public void stateChanged() {
		GameState state = api.currentGameState();
		switch (state) {
		case LEVEL_LOADED:
			gameView.drawLevelLoadedScreen(api.getLevelNumber());
			break;
		case LEVEL_RUNNING:
			gameView.startLevelRender(api.getRenderDetails());
			break;
		case LEVEL_COMPLETE:
			gameView.drawLevelCompleteScreen(api.getLevelNumber());
			break;
		case GAME_OVER:
			gameView.drawGameOverScreen();
			break;
		case GAME_COMPLETE:
			gameView.drawGameCompleteScreen();
			break;
		}
	}
	
}
