package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import model.api.Api;

public class KeyboardHandler implements KeyListener {
	
	public final static int ACCELERATE_KEY = KeyEvent.VK_UP;
	public final static int RIGHT_KEY = KeyEvent.VK_RIGHT;
	public final static int LEFT_KEY = KeyEvent.VK_LEFT;
	public final static int START_KEY = KeyEvent.VK_ENTER;
	public final static int SHOOT_KEY = KeyEvent.VK_SPACE;
	public final static int CHEAT_KEY = KeyEvent.VK_C;
	
	private Api api;
	
	public KeyboardHandler(Api api) {
		this.api = api;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case ACCELERATE_KEY:
			api.acceleratePlayer(true);
			break;
		case RIGHT_KEY:
			api.rotatePlayerClockwise(true);
			break;
		case LEFT_KEY:
			api.rotatePlayerCounterClockwise(true);
			break;
		case START_KEY:
			break;
		case SHOOT_KEY:
			api.shoot(true);
			break;
		case CHEAT_KEY:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case ACCELERATE_KEY:
			api.acceleratePlayer(false);
			break;
		case RIGHT_KEY:
			api.rotatePlayerClockwise(false);
			break;
		case START_KEY:
			api.start();
			break;
		case LEFT_KEY:
			api.rotatePlayerCounterClockwise(false);
			break;
		case SHOOT_KEY:
			api.shoot(false);
			break;
		case CHEAT_KEY:
			api.cheat();
			break;
		}
	}
	
}
