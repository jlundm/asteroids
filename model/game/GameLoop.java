package model.game;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import model.actor.Actor;
import model.actor.Asteroid;
import model.actor.LaserShot;
import model.actor.Player;
import model.actor.Ufo;
import model.api.ActorRenderDetails;
import model.api.ActorType;
import model.levels.Level;

public class GameLoop {
	private Level level;
	private boolean gameOver, levelComplete;
	private ConcurrentHashMap<Actor, ActorRenderDetails> actorRenderDetails;

	private final static long FIRE_RATE = 150;
	private long playerLastFiredTimestamp = 0;
	
	
	/*
	 * 
	 * INTERFACE
	 * 
	 */

	public GameLoop(Level level, ConcurrentHashMap<Actor, ActorRenderDetails> actorRenderDetails) {
		this.level = level;
		this.actorRenderDetails = actorRenderDetails;
		initiateRenderDetails();
	}

	public void tick(double tickTime) {
		// Pretty much entire game loop
		updatePlayer(tickTime);
		updateLasers(tickTime);
		updateAsteroids(tickTime);
		spawnUfos(tickTime);
		updateUfos(tickTime);
		firePlayerLasers(tickTime);
		updateRenderDetails();
		checkForWin();
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public boolean isLevelComplete() {
		return levelComplete;
	}

	public void cheat() {
		List<Ufo> ufos = level.getUfos();
		removeUfos(ufos);
		List<Asteroid> asteroids = level.getAsteroids();
		removeAsteroids(asteroids);
		checkForWin();
	}

	/*
	 * 
	 * GAME LOGIC
	 * 
	 */

	private void updatePlayer(double tickTime) {
		Player player = level.getPlayer();
		player.tick(tickTime);
	}

	private void updateLasers(double tickTime) {
		Player player = level.getPlayer();

		// Update existing lasers
		List<LaserShot> laserShotsToBeRemoved = new LinkedList<LaserShot>();
		for (LaserShot laserShot : level.getLaserShots()) {
			laserShot.tick(tickTime);

			// Remove if out of bounds
			if (laserShot.isOutOfBounds()) {
				laserShotsToBeRemoved.add(laserShot);
			}

			// Check player shots for collisions with asteroids and enemies
			else if (laserShot.gotFiredBy(player)) {
				List<Asteroid> asteroidsToBeRemoved = new LinkedList<Asteroid>();
				List<Asteroid> asteroidsToBeAdded = new LinkedList<Asteroid>();

				List<Asteroid> asteroids = level.getAsteroids();
				for (Asteroid asteroid : asteroids) {
					if (laserShot.collision(asteroid)) {
						asteroidsToBeRemoved.add(asteroid);

						List<Asteroid> children = asteroid.breakUp();

						for (Asteroid child : children) {
							if (!asteroidsToBeAdded.contains(child)) {
								asteroidsToBeAdded.add(child);
							}
						}

						laserShotsToBeRemoved.add(laserShot);
					}
				}

				if (!asteroidsToBeAdded.isEmpty()) {
					addAsteroids(asteroidsToBeAdded);
				}

				removeAsteroids(asteroidsToBeRemoved);

				// Kolla kollision med fiender
				List<Ufo> ufosToBeRemoved = new LinkedList<Ufo>();
				for (Ufo ufo : level.getUfos()) {
					if (laserShot.collision(ufo)) {
						ufosToBeRemoved.add(ufo);
					}
				}
				removeUfos(ufosToBeRemoved);
			} else {
				// Check enemy shots for collisions with player
				if (laserShot.collision(player)) {
					gameOver();
				}
			}
		}
		removeLaserShots(laserShotsToBeRemoved);
	}

	private void updateAsteroids(double tickTime) {
		Player player = level.getPlayer();
		for (Asteroid asteroid : level.getAsteroids()) {
			asteroid.tick(tickTime);

			// Check collisions with player
			if (asteroid.collision(player)) {
				gameOver();
			}
		}
	}

	private void spawnUfos(double tickTime) {
		// Spawn UFOs, or update ufo spawner time
		if (level.getUfos().isEmpty() && level.getUfoSpawner().hasUfos()) {
			List<Ufo> ufos = level.getUfoSpawner().spawnUfos();
			addUfos(ufos);
		} else if (level.getUfos().isEmpty()) {
			level.getUfoSpawner().tick(tickTime);
		}
	}

	private void updateUfos(double tickTime) {
		Player player = level.getPlayer();

		// Update UFOs, check for player collisions, fire lasers
		List<Ufo> ufos = level.getUfos();
		for (Ufo ufo : ufos) {
			ufo.tick(tickTime);
			if (ufo.collision(player)) {
				gameOver();
			}
			if (ufo.laserCharged()) {
				LaserShot laserShot = ufo.fireLaser(player);
				addLaserShot(laserShot);
			}
		}
	}

	private void firePlayerLasers(double tickTime) {
		Player player = level.getPlayer();
		// Fire player lasers
		long timeSinceLastShot = System.currentTimeMillis() - playerLastFiredTimestamp;
		if (player.isShooting() && FIRE_RATE <= timeSinceLastShot) {
			LaserShot laserShot = LaserShot.getLaserShot(player);
			addLaserShot(laserShot);
			playerLastFiredTimestamp = System.currentTimeMillis();
		}
	}

	private void checkForWin() {
		// Check win condition
		if (level.getAsteroids().isEmpty() && level.getUfos().isEmpty()) {
			levelComplete = true;
		}
	}

	private void gameOver() {
		gameOver = true;
	}

	/*
	 * 
	 * ADD/REMOVE ACTORS
	 * 
	 */

	private void addLaserShot(LaserShot laserShot) {
		level.getLaserShots().add(laserShot);

		ActorRenderDetails laserRenderDetails = new ActorRenderDetails(laserShot, ActorType.LASER_SHOT);
		synchronized (actorRenderDetails) {
			actorRenderDetails.put(laserShot, laserRenderDetails);
		}
	}

	private void addAsteroids(List<Asteroid> asteroids) {
		level.getAsteroids().addAll(asteroids);

		synchronized (actorRenderDetails) {
			for (Asteroid asteroid : asteroids) {
				ActorRenderDetails asteroidDetails = new ActorRenderDetails(asteroid, ActorType.ASTEROID);
				actorRenderDetails.put(asteroid, asteroidDetails);
			}
		}
	}

	private void addUfos(List<Ufo> ufos) {
		synchronized (actorRenderDetails) {
			for (Ufo ufo : ufos) {
				ActorRenderDetails ufoDetails = new ActorRenderDetails(ufo, ActorType.UFO);
				actorRenderDetails.put(ufo, ufoDetails);
			}
		}
		level.setUfos(ufos);
	}

	private void removeUfos(List<Ufo> ufos) {
		synchronized (actorRenderDetails) {
			for (Ufo ufo : ufos) {
				actorRenderDetails.remove(ufo);
			}
		}
		level.getUfos().removeAll(ufos);
	}

	private void removeAsteroids(List<Asteroid> asteroids) {
		synchronized (actorRenderDetails) {
			for (Asteroid asteroid : asteroids) {
				actorRenderDetails.remove(asteroid);
			}
		}
		level.getAsteroids().removeAll(asteroids);
	}

	private void removeLaserShots(List<LaserShot> laserShots) {
		synchronized (actorRenderDetails) {
			for (LaserShot laserShot : laserShots) {
				actorRenderDetails.remove(laserShot);
			}
		}
		level.getLaserShots().removeAll(laserShots);
	}

	/*
	 * 
	 * RENDER DETAILS
	 * 
	 */

	private void initiateRenderDetails() {
		ActorRenderDetails playerDetails = new ActorRenderDetails(level.getPlayer(), ActorType.PLAYER);
		actorRenderDetails.put(level.getPlayer(), playerDetails);

		for (Asteroid asteroid : level.getAsteroids()) {
			ActorRenderDetails asteroidDetails = new ActorRenderDetails(asteroid, ActorType.ASTEROID);
			actorRenderDetails.put(asteroid, asteroidDetails);
		}
	}

	private void updateRenderDetails() {
		// Update player details
		Player player = level.getPlayer();
		actorRenderDetails.get(player).update(player);

		// Update asteroid details
		for (Asteroid asteroid : level.getAsteroids()) {
			actorRenderDetails.get(asteroid).update(asteroid);
		}

		for (LaserShot laserShot : level.getLaserShots()) {
			actorRenderDetails.get(laserShot).update(laserShot);
		}

		for (Ufo ufo : level.getUfos()) {
			actorRenderDetails.get(ufo).update(ufo);
		}
	}

}
