package model.game;


import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import model.Settings;
import model.actor.*;
import model.api.ActorRenderDetails;
import model.api.GameStateChangeObserver;
import model.game.gamestate.GameOverState;
import model.game.gamestate.GameState;
import model.game.gamestate.LevelCompleteState;
import model.game.gamestate.LevelLoadedState;
import model.levels.*;

public class Game {
	private Timer gameLoopTimer;
	private GameLoopTimerTask gameLoopTimerTask;
	private GameLoop gameLoop;
	private long lastTickTime;
	private Level currentLevel;
	private LinkedList<LevelBuilder> levelBuilders;
	private ConcurrentHashMap<Actor, ActorRenderDetails> actorRenderDetails;
	private GameState state;
	private List<GameStateChangeObserver> gameStateChangeObservers;
	private int levelCounter;
	
	private class GameLoopTimerTask extends TimerTask {
		@Override
		public void run() {
			tick();
		}
	}
	
	/*
	 * 
	 *			PRIVATE METHODS
	 *
	 */
	
	private void tick() {
		double tickTime = advanceTickTime();
		gameLoop.tick(tickTime);
		if (gameLoop.isGameOver()) {
			setState(new GameOverState(this));
		} else if (gameLoop.isLevelComplete()) {
			setState(new LevelCompleteState(this));
		}
	}
	
	/**
	 * Advances the tick timer and returns number of seconds since last tick.
	 * Should be used every tick() for calculating animations, movement, etc.
	 * @return Number of seconds since last tick
	 */
	private double advanceTickTime() {
		long currentTickTime = System.currentTimeMillis();
		double delta = (currentTickTime - lastTickTime)/1000.0;
		lastTickTime = currentTickTime;
		System.out.println("Game frame time: " + delta);
		return delta;
	}
	
	/*
	 * 
	 * 			PUBLIC METHODS
	 * 
	 */

	public Game() {
		gameStateChangeObservers = new LinkedList<GameStateChangeObserver>();
		resetGame();
	}

	public void resetGame() {
		levelCounter = 0;
		// Ensure game is paused
		pauseGameLoop();

		// Add levelbuilders
		levelBuilders = new LinkedList<LevelBuilder>();
		//levelBuilders.add(new TestLevelBuilder());
		levelBuilders.add(new LevelOneBuilder());
		levelBuilders.add(new LevelTwoBuilder());
		levelBuilders.add(new LevelThreeBuilder());
		levelBuilders.add(new LevelFourBuilder());
		levelBuilders.add(new LevelFiveBuilder());
		// Load first level
		loadNextLevel();
		
		// Set state to loaded
		setState(new LevelLoadedState(this));
	}
	
	public boolean hasNextLevel() {
		return !levelBuilders.isEmpty();
	}
	
	public Collection<ActorRenderDetails> getRenderDetails() {
		return actorRenderDetails.values();
	}
	
	public void addGameStateChangeObserver(GameStateChangeObserver observer) {
		this.gameStateChangeObservers.add(observer);
	}
	
	public void removeGameStateChangeObserver(GameStateChangeObserver observer) {
		gameStateChangeObservers.remove(observer);
	}
	
	public int getLevelNumber() {
		return levelCounter;
	}
	
	public Level getCurrentLevel() {
		return currentLevel;
	}
	
	public void cheat() {
		this.gameLoop.cheat();
	}

	public void startGameLoop() {
		if ((gameLoopTimer == null) && gameLoop != null) {
			gameLoopTimer = new Timer("Game model thread");
			advanceTickTime();
			gameLoopTimerTask = new GameLoopTimerTask();
			gameLoopTimer.scheduleAtFixedRate(gameLoopTimerTask, 0, (1000/Settings.getInstance().getGameHz()));
		}
	}

	public void pauseGameLoop() {
		if (gameLoopTimer != null) {
			gameLoopTimer.cancel();
			gameLoopTimer = null;
		}
	}
	
	public void loadNextLevel() {
		levelCounter++;
		LevelBuilder levelBuilder = levelBuilders.remove();
		currentLevel = levelBuilder.buildLevel();
		actorRenderDetails = new ConcurrentHashMap<Actor, ActorRenderDetails>();
		gameLoop = new GameLoop(currentLevel, actorRenderDetails);
	}
	
	public void setState(GameState state) {
		this.state = state;
		notifyGameStateChangeObservers();
	}
	
	public GameState getState() {
		return state;
	}
	
	private void notifyGameStateChangeObservers() {
		for (GameStateChangeObserver observer : gameStateChangeObservers) {
			observer.stateChanged();
		}
	}
}
