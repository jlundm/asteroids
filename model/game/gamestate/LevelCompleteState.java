package model.game.gamestate;

import model.api.GameStateChangeObserver;
import model.game.Game;

public class LevelCompleteState extends GameState {

	public LevelCompleteState(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void initiateState() {
		getGame().pauseGameLoop();
	}

	@Override
	public void start() {
		Game game = getGame();
		if (game.hasNextLevel()) {
			game.loadNextLevel();
			game.setState(new LevelLoadedState(game));
		} else {
			game.setState(new GameCompleteState(game));
		}
	}

	@Override
	public void acceleratePlayer(boolean accelerate) {
	}

	@Override
	public void rotatePlayerClockwise(boolean rotate) {
	}

	@Override
	public void rotatePlayerCounterClockwise(boolean rotate) {
	}

	@Override
	public model.api.GameStateChangeObserver.GameState currentGameState() {
		return GameStateChangeObserver.GameState.LEVEL_COMPLETE;
	}

	@Override
	public void shoot(boolean shoot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cheat() {
		// TODO Auto-generated method stub
		
	}

	

}
