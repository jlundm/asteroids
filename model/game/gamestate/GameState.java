package model.game.gamestate;

import model.api.GameStateChangeObserver;
import model.game.Game;

public abstract class GameState {
	private Game game;
	
	public GameState(Game game) {
		this.game = game;
		initiateState();
	}
	
	public abstract void initiateState();
	public abstract void start();
	public abstract void acceleratePlayer(boolean accelerate);
	public abstract void rotatePlayerClockwise(boolean rotate);
	public abstract void rotatePlayerCounterClockwise(boolean rotate);
	public abstract void shoot(boolean shoot);
	public abstract void cheat();
	public abstract GameStateChangeObserver.GameState currentGameState();

	public Game getGame() {
		return game;
	}
}
