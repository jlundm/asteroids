package model.game.gamestate;

import model.api.GameStateChangeObserver;
import model.game.Game;

public class GameCompleteState extends GameState {

	public GameCompleteState(Game game) {
		super(game);
	}

	@Override
	public void start() {
		getGame().resetGame();
	}

	@Override
	public void acceleratePlayer(boolean accelerate) {
	}

	@Override
	public void rotatePlayerClockwise(boolean rotate) {
	}

	@Override
	public void rotatePlayerCounterClockwise(boolean rotate) {
	}

	@Override
	public void shoot(boolean shoot) {
	}

	@Override
	public void cheat() {
	}

	@Override
	public model.api.GameStateChangeObserver.GameState currentGameState() {
		return GameStateChangeObserver.GameState.GAME_COMPLETE;
	}

	@Override
	public void initiateState() {
		// TODO Auto-generated method stub
		
	}

}
