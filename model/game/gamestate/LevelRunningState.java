package model.game.gamestate;


import model.api.GameStateChangeObserver;
import model.game.Game;
import model.levels.Level;

public class LevelRunningState extends GameState {

	public LevelRunningState(Game game) {
		super(game);
	}

	@Override
	public void start() {
	}

	@Override
	public void acceleratePlayer(boolean accelerate) {
		Level currentLevel = getGame().getCurrentLevel();
		currentLevel.getPlayer().accelerate(accelerate);
	}

	@Override
	public void rotatePlayerClockwise(boolean rotate) {
		Level currentLevel = getGame().getCurrentLevel();
		currentLevel.getPlayer().rotateClockwise(rotate);
	}

	@Override
	public void rotatePlayerCounterClockwise(boolean rotate) {
		Level currentLevel = getGame().getCurrentLevel();
		currentLevel.getPlayer().rotateCounterClockwise(rotate);
	}

	@Override
	public model.api.GameStateChangeObserver.GameState currentGameState() {
		return GameStateChangeObserver.GameState.LEVEL_RUNNING;
	}

	@Override
	public void shoot(boolean shoot) {
		getGame().getCurrentLevel().getPlayer().setShooting(shoot);
	}

	@Override
	public void cheat() {
		getGame().cheat();
	}

	@Override
	public void initiateState() {
		
	}
	
}
