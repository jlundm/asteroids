package model.game.gamestate;

import model.api.GameStateChangeObserver;
import model.game.Game;

public class LevelLoadedState extends GameState {


	public LevelLoadedState(Game game) {
		super(game);
	}

	@Override
	public void start() {
		getGame().startGameLoop();
		getGame().setState(new LevelRunningState(getGame()));
	}

	@Override
	public void acceleratePlayer(boolean accelerate) {
		
	}

	@Override
	public void rotatePlayerClockwise(boolean rotate) {
		
	}

	@Override
	public void rotatePlayerCounterClockwise(boolean rotate) {
		
	}

	@Override
	public model.api.GameStateChangeObserver.GameState currentGameState() {
		// TODO Auto-generated method stub
		return GameStateChangeObserver.GameState.LEVEL_LOADED;
	}

	@Override
	public void shoot(boolean shoot) {
		
	}

	@Override
	public void cheat() {
		
	}

	@Override
	public void initiateState() {
		
	}

}
