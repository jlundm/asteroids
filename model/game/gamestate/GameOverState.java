package model.game.gamestate;

import model.api.GameStateChangeObserver;
import model.game.Game;

public class GameOverState extends GameState {

	public GameOverState(Game game) {
		super(game);
	}
	
	@Override
	public void initiateState() {
		getGame().pauseGameLoop();
	}

	@Override
	public void start() {
		getGame().resetGame();
	}

	@Override
	public void acceleratePlayer(boolean accelerate) {
		
	}

	@Override
	public void rotatePlayerClockwise(boolean rotate) {
	}

	@Override
	public void rotatePlayerCounterClockwise(boolean rotate) {
	}

	@Override
	public model.api.GameStateChangeObserver.GameState currentGameState() {
		return GameStateChangeObserver.GameState.GAME_OVER;
	}

	@Override
	public void shoot(boolean shoot) {
	}

	@Override
	public void cheat() {
	}

	

}
