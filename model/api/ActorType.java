package model.api;

public enum ActorType {
	PLAYER, ASTEROID, UFO, LASER_SHOT
}
