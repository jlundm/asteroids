package model.api;

import model.actor.Actor;

public class ActorRenderDetails {
	private boolean render;
	private double x, y, collisionRadius, rotation;
	private ActorType actorType;
	
	public ActorRenderDetails(ActorType actorType) {
		this.actorType = actorType;
	}
	
	public ActorRenderDetails(Actor actor, ActorType actorType) {
		this(actorType);
		x = actor.getPosition().getX();
		y = actor.getPosition().getY();
		collisionRadius = actor.getCollisionRadius();
		rotation = actor.getRotation();
		render = true;
	}
	
	public void update(Actor actor) {
		x = actor.getPosition().getX();
		y = actor.getPosition().getY();
		rotation = actor.getRotation();
	}
	
	public boolean render() {
		return render;
	}
	public void setRender(boolean render) {
		this.render = render;
	}
	
	public ActorType getActorType() {
		return actorType;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getCollisionRadius() {
		return collisionRadius;
	}
	public void setCollisionRadius(double collisionRadius) {
		this.collisionRadius = collisionRadius;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}
	
}
