package model.api;

public interface GameStateChangeObserver {
	enum GameState {
		LEVEL_LOADED, LEVEL_RUNNING, LEVEL_COMPLETE, GAME_OVER, GAME_COMPLETE
	}
	
	public void stateChanged();
	
}
