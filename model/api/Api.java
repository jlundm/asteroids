package model.api;

import java.util.Collection;

import model.game.Game;

public class Api {
	private Game game;
	
	public Api() {
		game = new Game();
	}
	
	public void start() {
		game.getState().start();
	}
	
	public void acceleratePlayer(boolean accelerate) {
		game.getState().acceleratePlayer(accelerate);
	}
	
	public void rotatePlayerClockwise(boolean rotate) {
		game.getState().rotatePlayerClockwise(rotate);
	}
	
	public void rotatePlayerCounterClockwise(boolean rotate) {
		game.getState().rotatePlayerCounterClockwise(rotate);
	}
	
	public void shoot(boolean shoot) {
		game.getState().shoot(shoot);
	}
	
	public void cheat() {
		game.getState().cheat();
	}
	
	public int getLevelNumber() {
		return game.getLevelNumber();
	}
	
	public Collection<ActorRenderDetails> getRenderDetails() {
		return game.getRenderDetails();
	}
	
	public GameStateChangeObserver.GameState currentGameState() {
		return game.getState().currentGameState();
	}
	
	public void addGameStateChangeObserver(GameStateChangeObserver observer) {
		game.addGameStateChangeObserver(observer);
	}
	
	public void removeGameStateChangeObserver(GameStateChangeObserver observer) {
		game.removeGameStateChangeObserver(observer);
	}
}
