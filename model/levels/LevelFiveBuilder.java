package model.levels;

import java.util.ArrayList;
import java.util.List;

import model.actor.Asteroid;

public class LevelFiveBuilder extends LevelBuilder {

	@Override
	protected List<Asteroid> makeAsteroids() {
		List<Asteroid> asteroids = new ArrayList<Asteroid>(21);
		asteroids.add(makeAsteroid(22, 27));
		asteroids.add(makeAsteroid(70, 40));
		asteroids.add(makeAsteroid(55, 23));
		asteroids.add(makeAsteroid(43, 73));
		asteroids.add(makeAsteroid(34, 19));
		asteroids.add(makeAsteroid(60, 29));
		asteroids.add(makeAsteroid(34, 22));
		asteroids.add(makeAsteroid(37, 11));
		asteroids.add(makeAsteroid(70, 30));
		asteroids.add(makeAsteroid(90, 60));
		
		asteroids.add(makeAsteroid(80, 15));
		asteroids.add(makeAsteroid(75, 31));
		asteroids.add(makeAsteroid(90, 22));
		asteroids.add(makeAsteroid(120, 15));
		asteroids.add(makeAsteroid(70, 14));
		
		asteroids.add(makeAsteroid(140, 12));
		asteroids.add(makeAsteroid(155, 27));
		asteroids.add(makeAsteroid(94, 9));
		asteroids.add(makeAsteroid(182, 30));
		asteroids.add(makeAsteroid(5, 150));
		asteroids.add(makeAsteroid(300, 22));
		return asteroids;
	}

	@Override
	protected UfoSpawner makeUfoSpawner() {
		return new UfoSpawner(15, 4, 140, 7, 3, false);
	}

}
