package model.levels;

import java.util.List;

import model.actor.Asteroid;
import model.actor.LaserShot;
import model.actor.Player;
import model.actor.Ufo;

public class Level {
	private List<Asteroid> asteroids;
	private Player player;
	private List<LaserShot> laserShots;
	private List<Ufo> ufos;
	private UfoSpawner ufoSpawner;
	
	public List<Ufo> getUfos() {
		return ufos;
	}
	public void setUfos(List<Ufo> ufos) {
		this.ufos = ufos;
	}
	public List<Asteroid> getAsteroids() {
		return asteroids;
	}
	public void setAsteroids(List<Asteroid> asteroids) {
		this.asteroids = asteroids;
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public List<LaserShot> getLaserShots() {
		return laserShots;
	}
	public void setLaserShots(List<LaserShot> laserShots) {
		this.laserShots = laserShots;
	}
	public UfoSpawner getUfoSpawner() {
		return ufoSpawner;
	}
	public void setUfoSpawner(UfoSpawner ufoSpawner) {
		this.ufoSpawner = ufoSpawner;
	}
	
}
