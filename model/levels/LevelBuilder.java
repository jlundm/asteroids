package model.levels;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import model.Point;
import model.Settings;
import model.actor.Asteroid;
import model.actor.LaserShot;
import model.actor.Player;
import model.actor.Ufo;

public abstract class LevelBuilder {
	
	public Level buildLevel() {
		Level level = new Level();
		
		level.setPlayer(makePlayer());
		level.setAsteroids(makeAsteroids());
		level.setLaserShots(new LinkedList<LaserShot>());
		level.setUfoSpawner(makeUfoSpawner());
		level.setUfos(new ArrayList<Ufo>(1));
		return level;
	}
	
	private Player makePlayer() {
		return new Player(Settings.getInstance().getGameWidth()/2, Settings.getInstance().getGameHeight()/2, 0, 0);
	};
	
	protected abstract List<Asteroid> makeAsteroids();
	protected abstract UfoSpawner makeUfoSpawner();
	
	/**
	 * Makes an asteroid of set size with set velocity in random direction, on a random edge coordinate.
	 * @param velocity
	 * @param size
	 * @return
	 */
	protected Asteroid makeAsteroid(double velocity, double size) {
		Point velocityVector = Point.getRandomDirectionVector(velocity);
		
		// Choose side of screen to spawn at, and put at random coord along edge
		Settings settings = Settings.getInstance();
		double wierdCoin = Math.random();
		double x;
		double y;
		if (wierdCoin < 0.25) {
			x = 0;
			y = random(0, settings.getGameHeight());
		} else if (wierdCoin < 0.50) {
			x = settings.getGameWidth();
			y = random(0, settings.getGameHeight());
		} else if (wierdCoin < 0.75) {
			x = random(0, settings.getGameWidth());
			y = 0;
		} else {
			x = random(0, settings.getGameWidth());
			y = settings.getGameHeight();
		}
		
		return new Asteroid(x, y, velocityVector.getX(), velocityVector.getY(), size);
	}
	
	protected double random(double min, double max) {
		return Math.random() * (max-min + 1.0) + min;
	}
}
