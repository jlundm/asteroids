package model.levels;

import java.util.ArrayList;
import java.util.List;

import model.actor.Asteroid;

public class LevelThreeBuilder extends LevelBuilder {

	@Override
	protected List<Asteroid> makeAsteroids() {
		List<Asteroid> asteroids = new ArrayList<Asteroid>(10);
		asteroids.add(makeAsteroid(22, 27));
		asteroids.add(makeAsteroid(70, 40));
		asteroids.add(makeAsteroid(55, 23));
		asteroids.add(makeAsteroid(43, 73));
		asteroids.add(makeAsteroid(34, 19));
		asteroids.add(makeAsteroid(12, 29));
		asteroids.add(makeAsteroid(11, 54));
		asteroids.add(makeAsteroid(19, 80));
		asteroids.add(makeAsteroid(38, 75));
		asteroids.add(makeAsteroid(57, 60));
		return asteroids;
	}

	@Override
	protected UfoSpawner makeUfoSpawner() {
		return new UfoSpawner(15, 2, 70, 5, 3, true);
	}

}
