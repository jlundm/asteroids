package model.levels;

import java.util.ArrayList;
import java.util.List;

import model.actor.Asteroid;

public class LevelFourBuilder extends LevelBuilder {

	@Override
	protected List<Asteroid> makeAsteroids() {
		List<Asteroid> asteroids = new ArrayList<Asteroid>(15);
		asteroids.add(makeAsteroid(105, 27));
		asteroids.add(makeAsteroid(70, 40));
		asteroids.add(makeAsteroid(130, 23));
		asteroids.add(makeAsteroid(43, 73));
		asteroids.add(makeAsteroid(110, 19));
		asteroids.add(makeAsteroid(60, 29));
		asteroids.add(makeAsteroid(70, 22));
		asteroids.add(makeAsteroid(89, 11));
		asteroids.add(makeAsteroid(140, 30));
		asteroids.add(makeAsteroid(90, 60));
		
		asteroids.add(makeAsteroid(80, 15));
		asteroids.add(makeAsteroid(75, 31));
		asteroids.add(makeAsteroid(90, 22));
		asteroids.add(makeAsteroid(120, 15));
		asteroids.add(makeAsteroid(70, 14));
		return asteroids;
	}

	@Override
	protected UfoSpawner makeUfoSpawner() {
		return new UfoSpawner(10, 3, 90, 15, 3, true);
	}

}
