package model.levels;

import java.util.ArrayList;
import java.util.List;

import model.Point;
import model.Settings;
import model.actor.Ufo;

public class UfoSpawner {
	private double spawnTime, lastSpawnedTime, velocity, rechargeTime;
	private int ufoSquadSize, shotsInVolley;
	private Point uniformDirectionVector;
	
	/**
	 * Spawns Ufos along x=0 with random Y coord. hasUfos() should be checked before each spawn.
	 * 
	 * @param spawnTime How often Ufo squad will spawn, in seconds
	 * @param ufoSquadSize How many Ufos in each squad
	 * @param velocity Velocity of the Ufos
	 * @param shotsInVolley Number of laser shots in each volley
	 * @param rechargeTime Time between laser volleys
	 * @param uniformDirections If true all Ufos go the same direction
	 */
	public UfoSpawner(double spawnTime, int ufoSquadSize, double velocity, int shotsInVolley, double rechargeTime, boolean uniformDirections) {
		this.spawnTime = spawnTime;
		this.ufoSquadSize = ufoSquadSize;
		this.velocity = velocity;
		this.shotsInVolley = shotsInVolley;
		this.rechargeTime = rechargeTime;
		if (uniformDirections) {
			uniformDirectionVector = Point.getVector(-15, velocity);
		}
	}
	
	/**
	 * Should be called each game tick to calculate spawn time.
	 * @param deltaTime
	 */
	public void tick(double deltaTime) {
		lastSpawnedTime += deltaTime;
	}
	
	public boolean hasUfos() {
		return spawnTime < lastSpawnedTime;
	}
	
	public List<Ufo> spawnUfos() {
		lastSpawnedTime = 0;
		return makeUfos();
	}
	
	/**
	 * Return ufos to be spawned.
	 * @return
	 */
	protected List<Ufo> makeUfos() {
		List<Ufo> ufos = new ArrayList<Ufo>(ufoSquadSize);
		for (int i = 0; i < ufoSquadSize; i++) {
			
			Point direction = uniformDirectionVector;
			if (direction == null) {
				direction = getSemiRandomVelocityVector();
			}
			
			Ufo ufo = new Ufo(0, getRandomYCoord(), direction.getX(), direction.getY());
			ufo.setRechargeTime(rechargeTime);
			ufo.setLaserShotsInVolley(shotsInVolley);
			ufos.add(ufo);
		}
		
		return ufos;
	}
	
	/**
	 * Returns random velocity vector ranging from -45 to 45 degrees.
	 * @return
	 */
	private Point getSemiRandomVelocityVector() {
		double angle = (Math.random() * 90.0) - 45.0;
		Point vector = Point.getVector(angle, velocity);
		return vector;
	}
	
	/**
	 * Returns random Y coordinate
	 * @return random Y coordinate
	 */
	private double getRandomYCoord() {
		return (Math.random() * Settings.getInstance().getGameHeight());
	}
}
