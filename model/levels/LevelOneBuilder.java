package model.levels;

import java.util.ArrayList;
import java.util.List;

import model.actor.Asteroid;

public class LevelOneBuilder extends LevelBuilder {

	@Override
	protected List<Asteroid> makeAsteroids() {
		ArrayList<Asteroid> asteroids = new ArrayList<Asteroid>(3);
		
		asteroids.add(makeAsteroid(30, 17));
		asteroids.add(makeAsteroid(20, 30));
		asteroids.add(makeAsteroid(60, 50));
		
		return asteroids;
	}

	@Override
	protected UfoSpawner makeUfoSpawner() {
		return new UfoSpawner(12, 2, 25, 3, 3, true);
	}

}
