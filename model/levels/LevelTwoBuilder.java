package model.levels;

import java.util.ArrayList;
import java.util.List;
import model.actor.Asteroid;

public class LevelTwoBuilder extends LevelBuilder {

	@Override
	protected List<Asteroid> makeAsteroids() {
		List<Asteroid> asteroids = new ArrayList<Asteroid>(5);
		asteroids.add(makeAsteroid(22, 110));
		asteroids.add(makeAsteroid(70, 36));
		asteroids.add(makeAsteroid(55, 60));
		asteroids.add(makeAsteroid(43, 73));
		asteroids.add(makeAsteroid(34, 19));
		return asteroids;
	}

	@Override
	protected UfoSpawner makeUfoSpawner() {
		return new UfoSpawner(15, 2, 30, 4, 3, true);
	}

}
