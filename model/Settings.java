package model;

public class Settings {
	
	private static Settings INSTANCE;
	private int gameWidth, gameHeight;
	private int renderHz, gameHz;
	
	private Settings() {
		gameWidth = 1280;
		gameHeight = 720;
		renderHz = gameHz = 144;
	}
	
	public static Settings getInstance() {
		if (INSTANCE == null)
			INSTANCE = new Settings();
		return INSTANCE;
	}

	public int getGameWidth() {
		return gameWidth;
	}

	public void setGameWidth(int gameWidth) {
		this.gameWidth = gameWidth;
	}

	public int getGameHeight() {
		return gameHeight;
	}

	public void setGameHeight(int gameHeight) {
		this.gameHeight = gameHeight;
	}
	
	public int getRenderHz() {
		return renderHz;
	}
	
	public int getGameHz() {
		return gameHz;
	}
	
}
