package model;

public class Point {
	private double x, y;
	
	public Point() {
		this(0, 0);
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public void setLocation(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Add two points and return as new point.
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static Point add(Point v1, Point v2) {
		return new Point(v1.getX() + v2.getX(), v1.getY() + v2.getY());
	}
	
	/**
	 * Subtract the second point from the first and return as new point.
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static Point subtract(Point v1, Point v2) {
		return new Point(v1.getX() - v2.getX(), v1.getY() - v2.getY());
	}
	
	/**
	 * Get vector with length and angle.
	 * @param angle Angle in degrees
	 * @param length Length of vector
	 * @return
	 */
	public static Point getVector(double angle, double length) {
		// Vector of supplied length, along x axis
		double x = length;
		double y = 0;
		
		// Rotate vector according to direction
		double radAngle = Math.toRadians(angle);
		double xPrim = (x * Math.cos(radAngle)) - (y * Math.sin(radAngle));
		double yPrim = (x * Math.sin(radAngle)) + (y * Math.cos(radAngle));
		
		return new Point(xPrim, yPrim);
	}
	
	public double length() {
		double x = this.getX();
		double y = this.getY();
		return Math.sqrt(x*x + y*y);
	}
	
	public double angle() {
		double angle = Math.atan2(y, x);
		return Math.toDegrees(angle);
	}
	
	public static double getAngle(Point from, Point to) {
		double angle = Math.atan2(to.getY() - from.getY(), to.getX() - from.getX()) * 180.0 / Math.PI;
		return angle;
	}
	
	public static double dotProduct(Point p1, Point p2) {
		return p1.getX() * p2.getX() + p1.getY() * p2.getY();
	}
	
	public static Point getRandomDirectionVector(double length) {
		double angle = Math.random() * 360.0;
		Point vector = Point.getVector(angle, length);
		return vector;
	}
}
