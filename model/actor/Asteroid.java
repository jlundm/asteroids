package model.actor;

import java.util.LinkedList;
import java.util.List;

import model.Point;

public class Asteroid extends Actor {
	
	private final static double BREAKUP_MIN_RADIUS = 18.0;
	private final static double BREAKUP_VELOCITY_BOOST = 50.0;

	public Asteroid(double xPos, double yPos, double xVel, double yVel, double radius) {
		super(xPos, yPos, xVel, yVel);
		setCollisionRadius(radius);
		setRotation(getVelocity().angle());
	}

	@Override
	public void tick(double tickTimeDelta) {
		Point newPos = nextPosition(tickTimeDelta);
		setPosition(newPos);
	}
	
	/**
	 * Return two new child asteroids, half of original size
	 * @return
	 */
	public List<Asteroid> breakUp() {
		List<Asteroid> asteroids = new LinkedList<Asteroid>();
		
		if (BREAKUP_MIN_RADIUS < getCollisionRadius()) {
			asteroids.add(getChild());
			asteroids.add(getChild());
		}
		
		return asteroids;
	}
	
	/**
	 * Returns a half-sized child asteroid with "random" velocity direction.
	 * @return
	 */
	private Asteroid getChild() {
		double randomAngle = Math.random() * 360.0;
		double speed = calculateChildVelocity(getVelocity().length(), getRotation(), randomAngle);
		Point velocity = Point.getVector(randomAngle, speed);
		
		double radius = getCollisionRadius()/2.0;
		
		Point positionOffset = Point.getVector(randomAngle, radius);
		Point position = Point.add(getPosition(), positionOffset);
		
		
		
		return new Asteroid(position.getX(), position.getY(), velocity.getX(), velocity.getY(), radius);
	}
	
	/**
	 * Den här kom jag på. Huh. Matte.
	 * @param oldVelocity
	 * @param oldRotation
	 * @param newRotation
	 * @return
	 */
	private double calculateChildVelocity(double oldVelocity, double oldRotation, double newRotation) {
		double angleDiff = floatMod(oldRotation - newRotation, 360.0);
		double speedReductionFactor = (-1.0/32400.0)*angleDiff*angleDiff;	// kvadratisk ekvation, f(0) = 0, f(180) = 1, f(360) = 0
		speedReductionFactor += (1.0/90.0) * angleDiff;
		double newVelocity = oldVelocity - (speedReductionFactor * (2.0/4.0) * oldVelocity) + (BREAKUP_VELOCITY_BOOST*speedReductionFactor);
		return newVelocity;
	}

}
