package model.actor;

import model.Point;

public class Player extends Actor {
	private final static double MAX_VELOCITY = 500;
	private final static double ROTATION_SPEED = 245;
	private final static double ACCELERATION = 500;
	private final static double VELOCITY_DECAY = ACCELERATION/7.0;
	
	private boolean accelerate, rotateClockwise, rotateCounterClockwise, shooting;
	
	public Player(double xPos, double yPos, double xVel, double yVel) {
		super(xPos, yPos, xVel, yVel);
		init();
	}

	public Player(double xPos, double yPos) {
		super(xPos, yPos);
		init();
	}
	
	private void init() {
		accelerate = rotateClockwise = rotateCounterClockwise = shooting = false;
		setMaxVelocity(MAX_VELOCITY);
		setCollisionRadius(10);
	}

	@Override
	public void tick(double tickTimeDelta) {
		
		if (rotateClockwise) {
			rotateClockwise(tickTimeDelta);
		}
		
		if (rotateCounterClockwise) {
			rotateCounterClockwise(tickTimeDelta);
		}
		
		if (accelerate) {
			accelerate(tickTimeDelta);
		} else {
			decayVelocity(tickTimeDelta);
		}
		
		Point newPos = nextPosition(tickTimeDelta);
		setPosition(newPos);
	}
	
	public void accelerate(boolean accelerate) {
		this.accelerate = accelerate;
	}
	
	public void rotateClockwise(boolean rotateClockwise) {
		this.rotateClockwise = rotateClockwise;
	}
	
	public void rotateCounterClockwise(boolean rotateCounterClockwise) {
		this.rotateCounterClockwise = rotateCounterClockwise;
	}
	
	private void accelerate(double deltaSec) {
		Point accelerationVector = Point.getVector(getRotation(), ACCELERATION*deltaSec);
		Point newVelocity = Point.add(getVelocity(), accelerationVector);
		setVelocity(newVelocity);
	}
	
	private void decayVelocity(double deltaSec) {
		double velocityMagnitude = getVelocity().length();
		double decayDelta = VELOCITY_DECAY*deltaSec;
		
		if (decayDelta < velocityMagnitude) {
			double velocityAngle = getVelocity().angle();
			Point newVelocity = Point.getVector(velocityAngle, getVelocity().length() - decayDelta);
			setVelocity(newVelocity);
		} else {
			setVelocity(new Point(0, 0));
		}
	}
	
	private void rotateClockwise(double deltaSec) {
		double rotation = getRotation() + (ROTATION_SPEED * deltaSec);
		setRotation(rotation);
	}
	
	private void rotateCounterClockwise(double deltaSec) {
		double rotation = getRotation() - (ROTATION_SPEED * deltaSec);
		setRotation(rotation);
	}
	
	public void setShooting(boolean shooting) {
		this.shooting = shooting;
	}
	
	public boolean isShooting() {
		return shooting;
	}
}
