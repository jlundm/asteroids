package model.actor;


import model.Point;
import model.Settings;

public abstract class Actor {
	
	private Point velocity, position;
	private double rotation, collisionRadius, maxVelocity;
	
	public Actor(double xPos, double yPos) {
		this(xPos, yPos, 0.0, 0.0);
	}
	
	public Actor(double xPos, double yPos, double xVel, double yVel) {
		position = new Point(xPos, yPos);
		velocity = new Point(xVel, yVel);
	}
	
	/**
	 * Called every game tick. Should update actor accordingly to tickTime.
	 * @param tickTimeDelta Time since last tick, in seconds.
	 */
	public abstract void tick(double tickTimeDelta);
	
	public Point getPosition() {
		return position;
	}
	
	public void setPosition(Point position) {
		Settings settings = Settings.getInstance();
		
		double x = floatMod(position.getX(), settings.getGameWidth());
		double y = floatMod(position.getY(), settings.getGameHeight());
		
		this.position = new Point(x, y);
	}
	
	public Point getVelocity() {
		return velocity;
	}
	public void setVelocity(Point velocity) {
		double velocityMagnitude = velocity.length();
		Point newVelocity = velocity;
		if (maxVelocity < velocityMagnitude) {
			newVelocity = Point.getVector(getRotation(), maxVelocity);
		}
		
		this.velocity = newVelocity;
	}
	public double getRotation() {
		return rotation;
	}
	public void setRotation(double rotation) {
		this.rotation = floatMod(rotation, 360.0);
	}
	public double getCollisionRadius() {
		return collisionRadius;
	}
	public void setCollisionRadius(double collisionRadius) {
		this.collisionRadius = collisionRadius;
	}
	
	public double getMaxVelocity() {
		return maxVelocity;
	}
	
	public void setMaxVelocity(double maxVelocity) {
		this.maxVelocity = maxVelocity;
	}
	
	public boolean collision(Actor actor) {
		Point otherPos = actor.getPosition();
		double otherRadius = actor.getCollisionRadius();
		
		double dx = position.getX() - otherPos.getX();
		double dy = position.getY() - otherPos.getY();
		double distance = Math.sqrt(dx * dx + dy * dy);
		
		return distance < collisionRadius + otherRadius;
	}
	
	/**
	 * Calculates a new position with current velocity * seconds.
	 * @return
	 */
	protected Point nextPosition(double deltaSec) {
		
		double xPos = position.getX() + (velocity.getX()*deltaSec);
		double yPos = position.getY() + (velocity.getY()*deltaSec);
		
		Point newPos = new Point(xPos, yPos);
		return newPos;
	}
	
	/**
	 * Shamelessly found at stackoverflow
	 * Thanks PianoMastR64
	 * @param x
	 * @param y
	 * @return
	 */
	protected double floatMod(double x, double y){
	    // x mod y behaving the same way as Math.floorMod but with doubles
	    return (x - Math.floor(x/y) * y);
	}
}
