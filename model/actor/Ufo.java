package model.actor;

import model.Point;

public class Ufo extends Actor {
	private final static double SIZE = 20.0;
	private final static double FIRE_RATE = 0.200;
	
	private double rechargeTime, lastFiredVolleyTime, lastFiredShotTime;
	private int shotsFiredInVolley, laserShotsInVolley;

	public Ufo(double xPos, double yPos, double xVel, double yVel) {
		super(xPos, yPos, xVel, yVel);
		setCollisionRadius(SIZE);
	}
	
	@Override
	public void tick(double tickTimeDelta) {
		// Move
		Point newPos = nextPosition(tickTimeDelta);
		setPosition(newPos);
		
		// Update shooting timers
		if (shotsFiredInVolley == laserShotsInVolley) {
			shotsFiredInVolley = 0;
			lastFiredVolleyTime = 0;
		} else {
			lastFiredVolleyTime += tickTimeDelta;
			lastFiredShotTime += tickTimeDelta;
		}
	}
	
	
	public double getRechargeTime() {
		return rechargeTime;
	}
	
	/**
	 * Time between firing laser shot volleys in seconds.
	 * @param rechargeTime
	 */
	public void setRechargeTime(double rechargeTime) {
		this.rechargeTime = rechargeTime;
	}
	
	public int getLaserShotsInVolley() {
		return laserShotsInVolley;
	}
	
	/**
	 * Number of laser shots fired in each volley.
	 * @param laserShotsInVolley
	 */
	public void setLaserShotsInVolley(int laserShotsInVolley) {
		this.laserShotsInVolley = laserShotsInVolley;
	}

	/**
	 * Get waiting laser shot.
	 * @param target
	 * @return
	 */
	public LaserShot fireLaser(Actor target) {
		double angle = Point.getAngle(getPosition(), target.getPosition());
		LaserShot laserShot = LaserShot.getLaserShot(this, angle);
		shotsFiredInVolley++;
		lastFiredShotTime = 0;
		return laserShot;
	}
	
	/**
	 * Returns true if the UFO is ready to fire.
	 * @return
	 */
	public boolean laserCharged() {
		return rechargeTime < lastFiredVolleyTime && shotsFiredInVolley < laserShotsInVolley && FIRE_RATE <= lastFiredShotTime;
	}

}
