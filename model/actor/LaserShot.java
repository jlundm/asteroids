package model.actor;

import model.Point;
import model.Settings;

public class LaserShot extends Actor {
	
	public final static double LASER_LENGTH = 15.0;
	private final static double COLLISION_RADIUS = 2.0;
	private final static double SPEED_OF_LIGHT = 650.0;
	private Actor firedBy;

	private boolean outOfBounds;

	private LaserShot(double xPos, double yPos, double xVel, double yVel) {
		super(xPos, yPos, xVel, yVel);
		outOfBounds = false;
		setCollisionRadius(COLLISION_RADIUS); // Its really line length
		setMaxVelocity(SPEED_OF_LIGHT);
	}
	
	/**
	 * Factory method. Summons a laser shot at appropriate distance from firing actor, velocity in the same direction actor is facing
	 * 
	 * @param firedBy
	 * @return
	 */
	public static LaserShot getLaserShot(Actor firedBy) {
		return getLaserShot(firedBy, firedBy.getRotation());
	}
	
	public static LaserShot getLaserShot(Actor firedBy, double angle) {
		Point positionOffset = Point.getVector(angle, LASER_LENGTH*2);
		Point laserPosition = Point.add(firedBy.getPosition(), positionOffset);
		Point velocity = Point.getVector(angle, SPEED_OF_LIGHT);
		LaserShot laserShot = new LaserShot(laserPosition.getX(), laserPosition.getY(), velocity.getX(), velocity.getY());
		laserShot.setFiredBy(firedBy);
		laserShot.setRotation(angle);
		return laserShot;
	}

	@Override
	public void tick(double tickTimeDelta) {
		Settings settings = Settings.getInstance();
		Point newPos = nextPosition(tickTimeDelta);
		if (newPos.getX() < 0 || settings.getGameWidth() < newPos.getX() ||
				newPos.getY() < 0 || settings.getGameHeight() < newPos.getY()) {
			outOfBounds = true;
		} else {
			setPosition(newPos);
		}
	}

	public boolean isOutOfBounds() {
		return outOfBounds;
	}
	
	public boolean gotFiredBy(Actor actor) {
		return this.firedBy == actor;
	}
	
	public Actor firedBy() {
		return firedBy;
	}

	public void setFiredBy(Actor firedBy) {
		this.firedBy = firedBy;
	}

}
