package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

import model.Settings;
import model.api.ActorRenderDetails;
import model.api.ActorType;
import view.models.ActorModel;
import view.models.AsteroidModel;
import view.models.LaserShotModel;
import view.models.PlayerModel;
import view.models.UfoModel;

public class GameView extends JPanel {
	
	private Collection<ActorRenderDetails> actorRenderDetails;
	private Timer renderLoopTimer;
	private RenderLoop renderLoop;
	private ActorModel playerModel, asteroidModel, laserShotModel, ufoModel;
	private long lastRenderTime;
	private boolean drawLevelLoaded, drawGameOver, drawLevelComplete, drawGameComplete;
	private Settings settings = Settings.getInstance();
	private int level;
	
	private class RenderLoop extends TimerTask {
		private GameView panel;
		
		public RenderLoop(GameView panel) {
			super();
			this.panel = panel;
		}
		
		@Override
		public void run() {
			long timeStamp = System.currentTimeMillis();
			double frameTime = (timeStamp - lastRenderTime)/1000.0;
			lastRenderTime = timeStamp;
			System.out.println("Render frame time: " + frameTime);
			panel.repaint();
		}
	}
	
	public GameView() {
		super();
		level = 0;
		this.setPreferredSize(new Dimension(settings.getGameWidth(), settings.getGameHeight()));
		drawLevelLoaded = drawLevelComplete = drawGameOver = false;
		this.setBackground(Color.black);
		this.setForeground(Color.green);
		renderLoop = new RenderLoop(this);
		playerModel = new PlayerModel();
		playerModel.drawCollisionRadius(false);
		laserShotModel = new LaserShotModel();
		asteroidModel = new AsteroidModel();
		ufoModel = new UfoModel();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if (actorRenderDetails != null) {	// Draw game
			synchronized (actorRenderDetails) {
				for (ActorRenderDetails renderDetails : actorRenderDetails) {
					if (renderDetails.render()) {
						switch(renderDetails.getActorType()) {
						case PLAYER:
							playerModel.draw(g, renderDetails);
							break;
						case ASTEROID:
							asteroidModel.draw(g, renderDetails);
							break;
						case LASER_SHOT:
							laserShotModel.draw(g, renderDetails);
							break;
						case UFO:
							ufoModel.draw(g, renderDetails);
							break;
						}
					}
				}
			}
		}
		
		if (drawGameOver) {
			String str = "GAME OVER";
			int strWidth = g.getFontMetrics().stringWidth(str);
			int strHeight = g.getFontMetrics().getHeight();
			g.drawChars(str.toCharArray(), 0, str.length(), (settings.getGameWidth()-strWidth)/2, (settings.getGameHeight()-strHeight)/2);
		}
		
		if (drawLevelLoaded) {
			String strTop = "LEVEL " + level;
			String strBottom = "PRESS ENTER TO START";
			int strWidthTop = g.getFontMetrics().stringWidth(strTop);
			int strWidthBottom = g.getFontMetrics().stringWidth(strBottom);
			int strHeight = g.getFontMetrics().getHeight();
			
			g.drawChars(strTop.toCharArray(), 0, strTop.length(), (settings.getGameWidth()-strWidthTop)/2, (settings.getGameHeight()-strHeight*2)/2);
			g.drawChars(strBottom.toCharArray(), 0, strBottom.length(), (settings.getGameWidth()-strWidthBottom)/2, (settings.getGameHeight()+strHeight)/2);
		}
		
		if (drawLevelComplete) {
			String strTop = "LEVEL " + level + " COMPLETE";
			String strBottom = "PRESS ENTER";
			int strWidthTop = g.getFontMetrics().stringWidth(strTop);
			int strWidthBottom = g.getFontMetrics().stringWidth(strBottom);
			int strHeight = g.getFontMetrics().getHeight();
			
			g.drawChars(strTop.toCharArray(), 0, strTop.length(), (settings.getGameWidth()-strWidthTop)/2, (settings.getGameHeight()-strHeight*2)/2);
			g.drawChars(strBottom.toCharArray(), 0, strBottom.length(), (settings.getGameWidth()-strWidthBottom)/2, (settings.getGameHeight()+strHeight)/2);
		}
		
		if (drawGameComplete) {
			String strTop = "GAME COMPLETED - YOU'RE WINNER";
			String strBottom = "PRESS ENTER";
			int strWidthTop = g.getFontMetrics().stringWidth(strTop);
			int strWidthBottom = g.getFontMetrics().stringWidth(strBottom);
			int strHeight = g.getFontMetrics().getHeight();
			
			g.drawChars(strTop.toCharArray(), 0, strTop.length(), (settings.getGameWidth()-strWidthTop)/2, (settings.getGameHeight()-strHeight*2)/2);
			g.drawChars(strBottom.toCharArray(), 0, strBottom.length(), (settings.getGameWidth()-strWidthBottom)/2, (settings.getGameHeight()+strHeight)/2);
		}
	}
	
	public void drawLevelLoadedScreen(int level) {
		stopLevelRender();
		actorRenderDetails = null;
		this.level = level;
		drawLevelComplete = drawGameOver = drawGameComplete = false;
		drawLevelLoaded = true;
		repaint();
	}
	
	public void drawLevelCompleteScreen(int level) {
		stopLevelRender();
		drawGameOver = drawLevelLoaded = drawGameComplete = false;
		drawLevelComplete = true;
		this.level = level;
		repaint();
	}
	
	public void drawGameOverScreen() {
		stopLevelRender();
		drawLevelLoaded = drawLevelComplete = drawGameComplete = false;
		drawGameOver = true;
		repaint();
	}
	
	public void drawGameCompleteScreen() {
		stopLevelRender();
		drawLevelLoaded = drawLevelComplete = drawGameOver = false;
		drawGameComplete = true;
		repaint();
	}
	
	public void startLevelRender(Collection<ActorRenderDetails> renderDetails) {
		stopLevelRender();
		if (renderDetails != null) {
			renderLoop = new RenderLoop(this);
			drawLevelLoaded = drawGameOver = drawLevelComplete = false;
			actorRenderDetails = renderDetails;
			renderLoopTimer = new Timer("Gamepanel refresh thread");
			renderLoopTimer.scheduleAtFixedRate(renderLoop, 0, (1000/Settings.getInstance().getRenderHz()));
		}
	}
	
	private void stopLevelRender() {
		if (renderLoopTimer != null) {
			renderLoopTimer.cancel();
			renderLoopTimer = null;
			renderLoop = null;
		}
	}
	
}
