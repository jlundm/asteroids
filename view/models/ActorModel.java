package view.models;

import java.awt.Graphics;

import model.api.ActorRenderDetails;


public interface ActorModel {
	public void draw(Graphics g, ActorRenderDetails renderDetails);
	public void drawCollisionRadius(boolean draw);
}
