package view.models;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import model.api.ActorRenderDetails;

public class AsteroidModel implements ActorModel {
	
	// POLYGON POINTS
	private final static double[] SMOOTH_ASTEROID_POINTS_X = { -1.0, -0.6,  0.0,  0.4,  0.8, 1.0, 0.80, 0.0, -1.0};
	private final static double[] SMOOTH_ASTEROID_POINTS_Y = { -0.4, -1.0, -1.0, -1.0, -0.8, 0.2, 0.95, 1.0,  0.6};
	private final static double[] CHIPPED_ASTEROID_POINTS_X = { -1.0, -0.8, -0.2,  0.0,  0.0,  0.6,  0.8,  1.0, 1.0, 0.6, 0.6, 0.2, -0.4, -0.8, -1.0};
	private final static double[] CHIPPED_ASTEROID_POINTS_Y = { -0.2, -0.6, -1.0, -0.6, -1.0, -1.0, -0.8, -0.2, 0.2, 0.4, 0.8, 1.0,  1.0, 0.8,  0.2};
	private final static double[] CRATERED_ASTEROID_POINTS_X = { -1.00, -0.85, -0.65, -0.35, -0.05,  0.25,  0.50,  0.75,  0.85,  0.90,  1.00, 1.00, 0.95, 0.85, 0.80, 0.75, 0.60, 0.20, -0.25, -0.65, -0.85, -0.95};
	private final static double[] CRATERED_ASTEROID_POINTS_Y = { -0.05, -0.50, -0.85, -0.95, -1.00, -1.00, -0.95, -0.80, -0.60, -0.30, -0.05, 0.05, 0.30, 0.40, 0.65, 0.75, 0.90, 1.00,  1.00,  0.80,  0.60,  0.15};
	
	@Override
	public void drawCollisionRadius(boolean draw) {
		
	}

	@Override
	public void draw(Graphics g, ActorRenderDetails renderDetails) {
		double x = renderDetails.getX();
		double y = renderDetails.getY();
		double radius = renderDetails.getCollisionRadius();
		if (renderDetails.hashCode() % 3 == 0) {
			Polygon smallAsteroid = makeSmoothAsteroidPolygon(x, y, radius);
			drawAsteroidPolygon(g, smallAsteroid);
		} else if (renderDetails.hashCode() % 3 == 1) {
			Polygon mediumAsteroid = makeChippedAsteroidPolygon(x, y, radius);
			drawAsteroidPolygon(g, mediumAsteroid);
		} else if (renderDetails.hashCode() % 3 == 2) {
			Polygon largeAsteroid = makeCrateredAsteroidPolygon(x, y, radius);
			drawAsteroidPolygon(g, largeAsteroid);
		}
		else {
			drawCircle(g, x, y, radius);
		}
		
		
	}
	
	private void drawAsteroidPolygon(Graphics g, Polygon p) {
		Color c = g.getColor();
		g.setColor(Color.gray);
		g.fillPolygon(p);
		
		g.setColor(Color.LIGHT_GRAY);
		g.drawPolygon(p);
		g.setColor(c);
	}
	
	private void drawCircle(Graphics g, double x, double y, double radius) {
		Color c = g.getColor();
		g.setColor(Color.gray);
		g.fillOval((int)(0.5+x-radius), (int)(0.5+y-radius), (int)(0.5+2*radius), (int)(0.5+2*radius));
		
		g.setColor(Color.white);
		g.drawOval((int)(0.5+x-radius), (int)(0.5+y-radius), (int)(0.5+2*radius), (int)(0.5+2*radius));
		g.setColor(c);
	}
	
	private Polygon makeSmoothAsteroidPolygon(double x, double y, double radius) {	
		int[] xi = scaleAndMoveToPlayer(SMOOTH_ASTEROID_POINTS_X, x, radius);
		int[] yi = scaleAndMoveToPlayer(SMOOTH_ASTEROID_POINTS_Y, y, radius);
		
		return new Polygon(xi, yi, xi.length);
	}
	
	private Polygon makeChippedAsteroidPolygon(double x, double y, double radius) {
		int[] xi = scaleAndMoveToPlayer(CHIPPED_ASTEROID_POINTS_X, x, radius);
		int[] yi = scaleAndMoveToPlayer(CHIPPED_ASTEROID_POINTS_Y, y, radius);
		
		return new Polygon(xi, yi, xi.length);
	}
	
	private Polygon makeCrateredAsteroidPolygon(double x, double y, double radius) {
		int[] xi = scaleAndMoveToPlayer(CRATERED_ASTEROID_POINTS_X, x, radius);
		int[] yi = scaleAndMoveToPlayer(CRATERED_ASTEROID_POINTS_Y, y, radius);
		
		return new Polygon(xi, yi, xi.length);
	}
	
	/**
	 * Scales an array of points to size, and pushes them toward coordinate.
	 * Returns ints.
	 * @param points
	 * @param coordinate
	 * @param radius
	 * @return
	 */
	private int[] scaleAndMoveToPlayer(double[] points, double coordinate, double radius) {
		int[] iPoints = new int[points.length];
		
		for (int i = 0; i < points.length; i++) {
			// Scale point to collision size
			double point = points[i] * radius;
			
			// Push to position
			point += coordinate;
			
			// Convert
			iPoints[i] = (int) point;
		}
		
		return iPoints;
	}
	
}
