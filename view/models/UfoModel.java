package view.models;

import java.awt.Color;
import java.awt.Graphics;

import model.api.ActorRenderDetails;

public class UfoModel implements ActorModel {
	private boolean drawCollisionRadius = false;
	
	@Override
	public void draw(Graphics g, ActorRenderDetails renderDetails) {
		double x = renderDetails.getX();
		double y = renderDetails.getY();
		double collisionRadius = renderDetails.getCollisionRadius();
		
		// Make ufo polygon around center
		double[] xp = { -1.5, 0, 1.5 };
		double[] yp = { 0.5, -0.7, 0.5 };
		
		double size = collisionRadius;
		
		for (int i = 0; i < 3; i++) {
			// Scale point to collision size
			xp[i] = xp[i]*size;
			yp[i] = yp[i]*size;
			
			// Push to position
			xp[i] = xp[i] + x;
			yp[i] = yp[i] + y;
		}
		
		// Convert to int
		int[] xi = new int[3];
		int[] yi = new int[3];
		
		for (int i = 0; i < 3; i++) {
			xi[i] = (int) xp[i];
			yi[i] = (int) yp[i];
		}
		
		// DRAW UFO
		
		Color defaultColor = g.getColor();
		
		// Draw ufo triangle
		g.setColor(Color.lightGray);
		g.fillPolygon(xi, yi, 3);
		g.setColor(Color.gray);
		g.drawPolygon(xi, yi, 3);
		// Draw cockpit globe
		g.setColor(Color.white);
		g.fillOval((int)(x-(collisionRadius/2)), (int)(y-collisionRadius), (int)(collisionRadius), (int)(collisionRadius));
		g.setColor(Color.red);
		g.drawOval((int)(x-(collisionRadius/2)), (int)(y-collisionRadius), (int)(collisionRadius), (int)(collisionRadius));
		g.setColor(defaultColor);
		// Draw collision radius
		if (drawCollisionRadius) {
			g.drawOval((int)(0.5+x-collisionRadius), (int)(0.5+y-collisionRadius), (int)(0.5+2*collisionRadius), (int)(0.5+2*collisionRadius));
		}
	}

	@Override
	public void drawCollisionRadius(boolean draw) {
		this.drawCollisionRadius = draw;
	}

}
