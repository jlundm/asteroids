package view.models;

import java.awt.Graphics;

import model.Point;
import model.api.ActorRenderDetails;

public class LaserShotModel implements ActorModel {
	
	private final static double LASER_LENGTH = 15.0;

	@Override
	public void draw(Graphics g, ActorRenderDetails renderDetails) {
		// Probably should have designed the actor positioning & size better......
		double inverseAngle = floatMod(renderDetails.getRotation()-180.0, 360.0);
		
		Point laserPos = new Point(renderDetails.getX(), renderDetails.getY());
		Point p1 = Point.add(laserPos, Point.getVector(inverseAngle, LASER_LENGTH));
		
		g.drawLine((int) p1.getX(), (int)p1.getY(), (int) renderDetails.getX(), (int) renderDetails.getY());
	}

	@Override
	public void drawCollisionRadius(boolean draw) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Shamelessly found at stackoverflow
	 * Thanks PianoMastR64
	 * @param x
	 * @param y
	 * @return
	 */
	private double floatMod(double x, double y){
	    // x mod y behaving the same way as Math.floorMod but with doubles
	    return (x - Math.floor(x/y) * y);
	}
}
