package view.models;

import java.awt.Color;
import java.awt.Graphics;

import model.api.ActorRenderDetails;

public class PlayerModel implements ActorModel {
	
	private boolean drawCollisionRadius = false;

	@Override
	public void drawCollisionRadius(boolean draw) {
		this.drawCollisionRadius = draw;
	}

	@Override
	public void draw(Graphics g, ActorRenderDetails renderDetails) {
		double x = renderDetails.getX();
		double y = renderDetails.getY();
		double collisionRadius = renderDetails.getCollisionRadius();
		double rotation = renderDetails.getRotation();
		
		if (drawCollisionRadius) {
			g.drawOval((int)(0.5+x-collisionRadius), (int)(0.5+y-collisionRadius), (int)(0.5+2*collisionRadius), (int)(0.5+2*collisionRadius));
		}
		
		// Make player polygon around center
		int[] xp = { -1, -1, 2 };
		int[] yp = { -1, 1, 0 };
		
		// Make necessary integers
		int size = (int) collisionRadius;
		int xi = (int) x;
		int yi = (int) y;		
		double radAngle = Math.toRadians(rotation);
		
		for (int i = 0; i < 3; i++) {
			// Scale point to collision size
			xp[i] = xp[i]*size;
			yp[i] = yp[i]*size;
			
			// Rotate point around center
			double xv = xp[i];
			double yv = yp[i];
			xp[i] = (int) (xv * Math.cos(radAngle) - yv*Math.sin(radAngle));
			yp[i] = (int) (xv * Math.sin(radAngle) + yv * Math.cos(radAngle));
			
			// Push to position
			xp[i] = xp[i] + xi;
			yp[i] = yp[i] + yi;
		}
		
		// Draw player
		g.fillPolygon(xp, yp, 3);
	}
	
}
