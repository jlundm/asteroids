package view;

import javax.swing.JFrame;


public class Window extends JFrame {
	
	public Window() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setVisible(true);
	    this.setResizable(false);
	}
}
